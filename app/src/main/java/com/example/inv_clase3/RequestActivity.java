package com.example.inv_clase3;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class RequestActivity extends AppCompatActivity implements Handler.Callback{

    private Handler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request);

        handler = new Handler(Looper.getMainLooper(), this);

        // pruebas de JSON
        String json1 = "{'nombre':'Juan', 'edad':21}";
        String json2 = "{'nombre':'Mariana', 'edad':22, 'mascota':{'nombre':'Firulais', 'especie':'perro'}}";
        String json3 = "{'nombre':'Sofia', 'calificaciones':[95, 92, 85]}";
        String json4 = "[{'nombre':'Pedro', 'edad':20}, {'nombre':'Maria', 'edad':21}, {'nombre':'Jonas', 'edad':19}]";

        try {

            JSONObject obj1 = new JSONObject(json1);
            Log.wtf("JSON", obj1.getString("nombre"));
            Log.wtf("JSON", obj1.getInt("edad") + "");

            JSONObject obj2 = new JSONObject(json2);
            JSONObject obj21 = obj2.getJSONObject("mascota");
            Log.wtf("JSON", obj21.getString("nombre"));
            Log.wtf("JSON", obj21.getString("especie"));

            JSONObject obj3 = new JSONObject(json3);
            JSONArray obj31 = obj3.getJSONArray("calificaciones");

            for(int i = 0; i < obj31.length(); i++){

                Log.wtf("JSON", obj31.getInt(i) + "");
            }

            JSONArray obj4 = new JSONArray(json4);

            for(int i = 0; i < obj4.length(); i++){

                JSONObject obj41 = obj4.getJSONObject(i);
                Log.wtf("JSON", "--------------------------------------");
                Log.wtf("JSON", obj41.getString("nombre"));
                Log.wtf("JSON", obj41.getInt("edad") + "");
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void hacerRequest(View v){

        Request request = new Request("https://api.github.com/users", handler);
        request.start();
    }

    @Override
    public boolean handleMessage(@NonNull Message msg) {

        try {
            JSONArray datos = (JSONArray) msg.obj;

            for (int i = 0; i < datos.length(); i++) {


                JSONObject temp = datos.getJSONObject(i);

                Log.wtf("JSON", "--------------------------------------");
                Log.wtf("JSON", temp.getString("login"));
                Log.wtf("JSON", temp.getInt("id") + "");
            }
        }catch (JSONException e) {
            e.printStackTrace();
        }

        return true;
    }
}