package com.example.inv_clase3;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class PerritosAdapter extends RecyclerView.Adapter<PerritosAdapter.PerritoViewHolder> {

    // definir un view holder
    // - un objeto que se encarga de administrar una vista de renglón
    public class PerritoViewHolder extends RecyclerView.ViewHolder {

        public TextView texto1, texto2;

        public PerritoViewHolder(@NonNull View itemView) {
            super(itemView);

            texto1 = itemView.findViewById(R.id.textView4);
            texto2 = itemView.findViewById(R.id.textView5);
        }
    }

    // extremadamente común, casi regla
    // referencia a fuente de datos
    private ArrayList<String> perritos;
    private View.OnClickListener listener;

    public PerritosAdapter(ArrayList<String> perritos, View.OnClickListener listener){
        this.perritos = perritos;
        this.listener = listener;
    }

    @NonNull
    @Override
    public PerritoViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        // crear view específica de renglón
        View v = (View) LayoutInflater.from(parent.getContext()).inflate(R.layout.row, parent, false);
        Button b = v.findViewById(R.id.button7);

        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.wtf("BOTONAZO", "BOTON PRESIONADO");
            }
        });

        v.setOnClickListener(listener);

        PerritoViewHolder pvh = new PerritoViewHolder(v);
        return pvh;
    }

    @Override
    public void onBindViewHolder(@NonNull PerritoViewHolder holder, int position) {

        // asocia un viewholder con datos en particular
        holder.texto1.setText(perritos.get(position));
        holder.texto2.setText(perritos.get(position));
    }

    @Override
    public int getItemCount() {
        return perritos.size();
    }


}
